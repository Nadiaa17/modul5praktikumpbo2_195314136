package Tugaspraktikum5;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Tugaspraktikum5 extends JFrame implements ActionListener {
    private static final int FRAME_WIDTH    = 350;
    private static final int FRAME_HEIGHT   = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT= 30;
    private JTextField text,text1,text2;
    private JLabel label,label1,label2;
    private JButton button;
public static void main(String[] args) {
      Tugaspraktikum5 frame = new Tugaspraktikum5();
      frame.setVisible(true);
   } 
    public Tugaspraktikum5(){
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        
        setSize(FRAME_WIDTH , FRAME_HEIGHT);
        setResizable (false);
        setTitle     ("   Luas Tanah");
        setLocation (FRAME_X_ORIGIN,FRAME_Y_ORIGIN);
        
        label = new JLabel("Panjang (m) :");
        label.setBounds(20,5,100,30);
        contentPane.add(label);
        
        text = new JTextField();
        text.setBounds(110,5,110,20);
        contentPane.add(text);
                 
        label1 = new JLabel("Lebar (m) :");
        label1.setBounds(20,40,110,30);
        contentPane.add(label1);
            
        text1 = new JTextField();
        text1.setBounds(110,40,110,20);
        contentPane.add(text1);
              
        label2 = new JLabel("Luas (m2) :");
        label2.setBounds(20,75,100,30);
        contentPane.add(label2);
        
        text2 = new JTextField();
        text2.setBounds(110,75,110,20);
        contentPane.add(text2);
                              
        button = new JButton("Hitung");
        button.setBounds(110,115,BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);

        button.addActionListener(this);
              
        setDefaultCloseOperation (EXIT_ON_CLOSE);
}
    @Override
    public void actionPerformed(ActionEvent e) {
    try{
    int bilangan1 = Integer.parseInt(text.getText());
    int bilangan2 = Integer.parseInt(text1.getText());
    int hasil = bilangan1 * bilangan2;
    text2.setText(Integer.toString(hasil));
    }
    catch(NumberFormatException d){
            JOptionPane.showMessageDialog(null, "Maaf, hanya integer yang diperbolehkan!","                          "
            + "Error", JOptionPane.ERROR_MESSAGE);
         }        
    }   
    }